#include "CaesarText.h"
#include <iostream>
const int CAESAR_KEY = 3;
CaesarText::CaesarText(std::string text) :ShiftText(text, CAESAR_KEY)
{

}


CaesarText::~CaesarText() = default;

std::string CaesarText::encrypt()
{
	return ShiftText::encrypt();
}

std::string CaesarText::decrypt()
{
	return  ShiftText::decrypt();
}
/*std::ostream& operator<<(std::ostream& os, ShiftText& other)
{
	os << "CaesarText\n";
	if (other.isEnc() == true)
		os << other.getText() << std::endl;
	else
		os << other.encrypt() << std::endl;


	return os;
}*/