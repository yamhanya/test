#pragma once

#include <string>
using namespace std;

class PlainText
{
protected:
	std::string text;
	bool isEncrypted;
	int _NumOfTexts;

	

public:
	 PlainText(std::string text);
	 PlainText();
	 ~PlainText();
	 bool isEnc();
	 std::string getText();
	 friend std::ostream& operator<<(std::ostream& os, PlainText& other);


	

};