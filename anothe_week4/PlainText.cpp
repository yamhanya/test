#include "PlainText.h"
#include <iostream>


PlainText::PlainText(std::string text)
{
	this->_NumOfTexts += 1;
	this->text = text;
	this->isEncrypted = false;
}

PlainText::PlainText() = default;

PlainText::~PlainText() = default;



bool PlainText::isEnc()
{
	return this->isEncrypted;
}

std::string PlainText::getText()
{
	return this->text;
}

std::ostream& operator<<(std::ostream& os, PlainText& other)
{
	os << "PlainText\n";
	os << other.getText() << std::endl;

	return os;
}
