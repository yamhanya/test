#include "ShiftText.h"
#include "PlainText.h"
#include <string>
#include <iostream>
using namespace std;

const char FIRST_SMALL_LETTER = 'a';
const char LAST_SMALL_LETTER = 'z';

ShiftText::ShiftText(std::string text, int key)
{
	this->text = text;
	this->_key = key;
	this->text = encrypt();
}

ShiftText::ShiftText() = default;
ShiftText::~ShiftText() = default;

std::string ShiftText::encrypt()
{
	int i = 0;
	char ch = this->text[i];
	for (i = 0; this->text[i] != '\0'; i++)
	{
		ch = this->text[i];
		if (ch == ',' || ch == ' ' || ch == '.')
		{
			this->text[i] = ch;
		}
		else if (ch >= FIRST_SMALL_LETTER and ch <= LAST_SMALL_LETTER)
		{
			ch+=this->_key;
			if (ch > LAST_SMALL_LETTER)
				ch = ch - LAST_SMALL_LETTER + FIRST_SMALL_LETTER - 1;
		}
		this->text[i] = ch;
	}
	this->isEncrypted = true;
	return this->text;
}

std::string ShiftText::decrypt()
{
	int i = 0;
	char ch = this->text[i];
	for (i = 0; this->text[i] != '\0'; i++)
	{
		ch = this->text[i];
		if (ch == ',' || ch == ' ' || ch == '.')
		{
			this->text[i] = ch;
		}
			//decrypt for lowercase letter
		else if (ch >= FIRST_SMALL_LETTER and ch <= LAST_SMALL_LETTER)
			ch = ch - this->_key;
		else if (ch < FIRST_SMALL_LETTER)
			ch = ch + LAST_SMALL_LETTER - FIRST_SMALL_LETTER + 1;
		this->text[i] = ch;
	}
	this->isEncrypted = false;
	return this->text;
}

std::ostream& operator<<(std::ostream& os, ShiftText& other)
{
	os << "ShiftText\n";
	if(other.isEnc()==true)
		os << other.getText() << std::endl;
	else
		os << other.encrypt() << std::endl;
	//other.isEncrypted= true;


	return os;
}
