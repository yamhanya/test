#pragma once

#include <string>
#include "ShiftText.h"
using namespace std;

class CaesarText :public ShiftText
{

public:
	CaesarText(std::string text);
	~CaesarText();
	std::string encrypt();
	std::string decrypt();
	//friend std::ostream& operator<<(std::ostream& os, ShiftText& other);



};