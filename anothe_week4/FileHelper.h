#pragma once

#include <string>
using namespace std;

class FileHelper
{
protected:



public:

	static std::string readFileToString(std::string fileName);
	static void writeWordsToFile(std::string inputFileName, std::string outputFileName);



};