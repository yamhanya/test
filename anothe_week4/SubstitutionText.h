#pragma once

#include <string>
#include "PlainText.h"
using namespace std;

class SubstitutionText :public PlainText
{
protected:
	std::string _dictionaryFileName;



public:
	SubstitutionText(std::string text, std::string dictionaryFileName);
	SubstitutionText();
	~SubstitutionText();
	std::string encrypt();
	std::string decrypt();



};