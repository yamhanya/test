#pragma once

#include <string>
#include "PlainText.h"
using namespace std;

class ShiftText:public PlainText
{
protected:
	int _key;



public:
	ShiftText(std::string text,int key);
	ShiftText();
	~ShiftText();
	std::string encrypt();
	std::string decrypt();
	friend std::ostream& operator<<(std::ostream& os, ShiftText& other);



};